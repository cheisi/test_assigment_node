"use strict";

var should = require('should');

var Server = require('../../lib/core/Server');
var DbMigrator = require("../../lib/core/DbMigrator");


// init

if (!process.env.DEBUG)
    process.env.SILENT = true;

var server = new Server();

server.init(process.cwd());


// migrate & truncate

before(function(done) {
    var dbMigrator = new DbMigrator(server, process.cwd());

    dbMigrator.run(function(error) {
        (!error).should.equal(true);

        server.db.query('truncate table users;', function(error) {
            (!error).should.equal(true);

            done();
        });
    });

});


module.exports = server;