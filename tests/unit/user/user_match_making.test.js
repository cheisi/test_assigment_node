"use strict";

var should = require('should');
var crypto = require('crypto');
var async = require('async');

var core = require('../Core');

var randomPrefix = crypto.createHash('md5').update(new Date().getTime().toString()).digest('hex') + '_';

var testCases = [
    // level, rank, opponent index, created user id
    [10, 25, 1],
    [9, 24, 0],
    [13, 26, 0],

    [10, 45, 4],
    [10, 43, 3],
    [10, 50, 3],
    [100, 100, 5]
];

describe('User match making', function() {

    before(function(done) {

        var email;

        var query = "insert into users (email, name, level, rank) values ";

        query += testCases.map(function(testCase, index) {
            email = "'" + randomPrefix + index + "'";
            return '(' + [email, email, testCase[0], testCase[1]].join(', ') + ')';
        }).join(' ,');

        query += ' returning id';

        core.db.query(query, function(error, rows) {
            (!error).should.equal(true);
            rows.length.should.equal(testCases.length);

            testCases.map(function(testCase, index) {
                testCase.push(rows[index].id);
            });

            done();
        });

    });

    describe('userMatchMaking', function() {

        it('should respond with error when user id is invalid', function(done) {
            core.dataGate.callQuery('userMatchMaking', [0], function(error, opponent) {

                (!!error).should.equal(true);
                error.should.be.a.String().and.equal('Wrong id');

                done();
            });
        });

        it('should respond with valid user ids', function(done) {

            var opponentIndex, opponentId, userId;

            async.eachLimit(testCases, 1, function(testCase, fn) {
                opponentIndex = testCase[2];
                opponentId = testCases[opponentIndex][3];
                userId = testCase[3];

                core.dataGate.callQuery('userMatchMaking', [userId], function(error, opponent) {
                    (!error).should.equal(true);

                    opponent.id.should.be.a.Number().and.equal(opponentId);

                    fn();
                });

            },  function(error) {
                (!error).should.equal(true);

                done();
            });

        });

    });

    after(function(done) {

        var query = 'delete from users where id in (';

        query += testCases.map(function(testCase) {
            return testCase[3];
        }).join(', ');

        query += ');';

        core.db.query(query, function(error) {
            (!error).should.equal(true);

            done();
        });

    });

});
