"use strict";

var should = require('should');
var crypto = require('crypto');
var async = require('async');

var core = require('../Core');

var randomName = new Date().getTime().toString();
var randomEmail = crypto.createHash('md5').update(randomName).digest('hex');
var userId;

describe('User simple queries', function() {

    describe('userGetById', function() {

        it('should respond with error when id is invalid', function(done) {

            core.dataGate.callQuery('userGetById', [0], function(error, user) {
                (!!error).should.equal(true);

                error.should.be.a.String().equal('Wrong id'); //error codes..

                done();
            });
        });

    });

    describe('userGetOrCreate', function() {

        it('should respond with error when email is empty', function(done) {

            core.dataGate.callQuery('userGetOrCreate', ['', ''], function(error, user) {
                (!!error).should.equal(true);

                error.should.be.a.String().equal('Email is empty');

                done();
            });

        });

        it('should create new user with "new" email', function (done) {

            core.dataGate.callQuery('userGetOrCreate', [randomEmail, randomName], function (error, user) {
                (!error).should.equal(true);

                user.should.be.a.Object().and.have.property('id');
                user.should.have.property('level').be.a.Number().equal(0);
                user.should.have.property('rank').be.a.Number().equal(0);
                user.should.have.property('name').be.a.String().equal(randomName);

                userId = user.id;

                done();
            });

        });

        it('should respond with same user second time', function (done) {

            core.dataGate.callQuery('userGetOrCreate', [randomEmail, randomName], function (error, user) {
                (!error).should.equal(true);

                user.should.be.a.Object().and.have.property('id').equal(userId);
                user.should.have.property('level').be.a.Number().equal(0);
                user.should.have.property('rank').be.a.Number().equal(0);
                user.should.have.property('name').be.a.String().equal(randomName);

                done();
            });

        });

    });

    describe('userChangeLevel', function() {

        it('should respond with error when id is invalid', function(done) {

            core.dataGate.callQuery('userChangeLevel', [0, +1], function(error, user) {
                (!!error).should.equal(true);

                error.should.be.a.String().equal('Wrong id');

                done();
            });
        });

        it('should respond with valid level when id is valid', function(done) {

            var sourceLevel;

            async.waterfall([

                function(next) {
                    core.dataGate.callQuery('userGetById', [userId], next);
                },

                function(user, next) {
                    sourceLevel = user.level;

                    core.dataGate.callQuery('userChangeLevel', [userId, +1], next);
                },

                function(level, next) {
                    level.should.be.a.Number().equal(sourceLevel + 1);

                    core.dataGate.callQuery('userChangeLevel', [userId, -4], next);
                },

                function(level, next) {
                    level.should.be.a.Number().equal(sourceLevel + 1 - 4);

                    next();
                }

            ],  function(error) {
                (!error).should.equal(true);

                done();
            });

        });

    });

    describe('userChangeRank', function() {

        it('should respond with error when id is invalid', function(done) {

            core.dataGate.callQuery('userChangeRank', [0, +1], function(error, user) {
                (!!error).should.equal(true);

                error.should.be.a.String().equal('Wrong id');

                done();
            });
        });

        it('should respond with valid rank when id is valid', function(done) {

            var sourceRank;

            async.waterfall([

                function(next) {
                    core.dataGate.callQuery('userGetById', [userId], next);
                },

                function(user, next) {
                    sourceRank = user.rank;

                    core.dataGate.callQuery('userChangeRank', [userId, +10], next);
                },

                function(rank, next) {
                    rank.should.be.a.Number().equal(sourceRank + 10);

                    core.dataGate.callQuery('userChangeRank', [userId, -5], next);
                },

                function(rank, next) {
                    rank.should.be.a.Number().equal(sourceRank + 10 - 5);

                    next();
                }

            ],  function(error) {
                (!error).should.equal(true);

                done();
            });

        });

    });

    after(function(done) {

        var query = "delete from users where email = '" + randomEmail + "'";

        core.db.query(query, function(error) {
            (!error).should.equal(true);

            done();
        });

    });

});
