"use strict";

//TODO jake?

var fs = require('fs');

var Config = require("./lib/core/Config");
var Logger = require("./lib/core/Logger");
var Db = require("./lib/core/Db");
var DbMigrator = require("./lib/core/DbMigrator");

var config = new Config();

var logsPath = process.cwd() + config.logger().path;

if (!fs.existsSync(logsPath))
    fs.mkdirSync(logsPath);

var Core = function() {
    this.config = config;
    this.logger = new Logger(config.logger());
    this.db = new Db(this);
};

var core = new Core();

core.logger.info('Start pre-deploy');

var dbMigrator = new DbMigrator(core, __dirname);

dbMigrator.run(function(error) {
    if (error)
        core.logger.error("Migration error:\r\n" + error.toString());

    process.exit();
});

