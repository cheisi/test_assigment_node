CREATE TABLE IF NOT EXISTS migrations (
    name CHARACTER VARYING(255),
    CONSTRAINT migrations_pkey PRIMARY KEY (name)
);
