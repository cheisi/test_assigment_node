CREATE TABLE users
(
     id SERIAL NOT NULL,
     name CHARACTER VARYING(256),
     email CHARACTER VARYING(256),
     level INTEGER,
     rank INTEGER,
     CONSTRAINT users_pkey PRIMARY KEY (id)
);

CREATE INDEX users_rank_level ON users (rank, level);