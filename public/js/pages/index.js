(function() {

    var request = function (apiMethod, data, fn) {
        $.post('/api/' + apiMethod, data, function(response) {

            $('.ajax-response pre').text(JSON.stringify(response, null, 2));

            if (response.error)
                return fn(response.error);

            fn(null, response.result);
        });
    };

    var change = function(field, value) {

        request('change-' + field, {value: value}, function(err, result) {
            if (err)
                return alert(err);

            $('.user-' + field).val(result);
        });

    };

    $(document).ready(function() {
        if (user) {
            $('.user-email').val(user.email);
            $('.user-name').val(user.name);
            $('.user-level').val(user.level);
            $('.user-rank').val(user.rank);

            $('.user-email-label').text(user.email);
        }

        $('.btn-level-inc').click(function() {
            change('level', 1);
        });

        $('.btn-level-dec').click(function() {
            change('level', -1);
        });

        $('.btn-rank-inc').click(function() {
            change('rank', 1);
        });

        $('.btn-rank-dec').click(function() {
            change('rank', -1);
        });

        $('.btn-match-making').click(function() {
            request('match-making', {}, function(err) {
                if (err)
                    return alert(err);
            });
        })
    });

})();