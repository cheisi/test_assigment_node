"use strict";

var async = require('async');

module.exports = function(id, value, fn) {

    var db = this.core.db;
    var logger = this.core.logger;

    if (!id)
        return fn('Wrong id');

    var query =
        "update users set" +
            " level = level + " + value +
            " where id = " + id +
            " returning level;";

    async.waterfall([

        function(next) {
            db.query(query, next);
        },

        function(rows, next) {
            if (!rows || !rows.length)
                return fn('Wrong id');

            next(null, rows[0].level);
        }

    ],  function(error, level) {
        if (error) {
            logger.error('Cant update user level: ' + error.toString());
            return fn(error);
        }

        fn(null, level);
    });

};