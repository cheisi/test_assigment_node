"use strict";

var async = require('async');

module.exports = function(id, fn) {

    var db = this.core.db;
    var logger = this.core.logger;

    var fields = ['id', 'email', 'name', 'level', 'rank'];

    if (!id)
        return fn('Wrong id');

    async.waterfall([

        // without sql-joins, do we have shards/partitions?

        function(next) {
            var query =
                "select " + fields.join(", ") +
                    " from users" +
                    " where id = " + id + ";";

            db.query(query, next);
        },

        function(rows, next) {
            if (!rows || !rows.length)
                return fn('Wrong id');

            var user = rows[0];

            var query =
                "select" +
                    " abs(" + user.rank + " - rank)," +
                    " abs(" + user.level + " - level)," +
                    " " + fields.join(", ") +
                    " from users where id <> " + id +
                    " order by 1, 2 asc limit 1;";

            db.query(query, next);
        },

        function(rows, next) {
            var user;

            if (rows && rows.length) {
                var row = rows[0];
                user = {};

                fields.map(function(field) {
                    user[field] = row[field];
                });
            }

            next(null, user);
        }

    ],  function(error, user) {
        if (error) {
            logger.error('Cant select opponent: ' + error.toString());
            return fn(error);
        }

        fn(null, user);
    });

};