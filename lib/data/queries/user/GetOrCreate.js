"use strict";

var async = require('async');

module.exports = function(email, name, fn) {

    if (!email)
        return fn('Email is empty');

    var db = this.core.db;
    var logger = this.core.logger;

    var fields = ['id', 'name', 'email', 'rank', 'level'];

    async.waterfall([

        function(next) {
            logger.info('Get user by email: ' + email);

            var query =
                "select " + fields.join(", ") +
                    " from users" +
                    " where email = '" + email + "';"

            db.query(query, next);
        },

        function(rows, next) {
            if (rows && rows.length)
                return next(null, rows); // update user name?

            logger.info('Create user with email: ' + email);

            var query =
                "insert into users (email, name) values (" +
                    "'" + email + "'," +
                    "'" + name + "'" +
                ") returning " + fields.join(", ");

            db.query(query, next);
        },

        function(rows, next) {
            if (!rows || !rows.length)
                return next('DB error');

            next(null, rows[0]);
        }

    ],  function(error, user) {
        if (error) {
            logger.error('Cant getOrCreate user: ' + error.toString());
            return fn(error)
        }

        fn(null, user);

    });

};