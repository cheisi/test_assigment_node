"use strict";

var async = require('async');

module.exports = function(id, fn) {

    var db = this.core.db;
    var logger = this.core.logger;

    var fields = ['id', 'name', 'email', 'rank', 'level'];

    var query =
        "select " + fields.join(", ") +
            " from users" +
            " where id = " + id + ";";

    async.waterfall([

        function(next) {
            db.query(query, next);
        },

        function(rows, next) {
            if (!rows || !rows.length)
                return fn('Wrong id'); //error codes..

            next(null, rows[0]);
        }

    ],  function(error, user) {
        if (error) {
            logger.error('Cant get user by id: ' + error.toString());
            return fn(error);
        }

        fn(null, user);
    });

};