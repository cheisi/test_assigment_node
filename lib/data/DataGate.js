"use strict";

var BaseQuery = require('./BaseQuery');

var DateGate = function(core) {
    var self = this;

    this.core = core;
    this.queries = {};

    var addQuery = function(name, handler) {
        if (name in self.queries)
            throw new Error('Query already exists');

        self.queries[name] = handler;
    };

    var initQueries = function() {

        // user
        addQuery('userGetById', require('./queries/user/GetById'));
        addQuery('userGetOrCreate', require('./queries/user/GetOrCreate'));
        addQuery('userChangeLevel', require('./queries/user/ChangeLevel'));
        addQuery('userChangeRank', require('./queries/user/ChangeRank'));

        addQuery('userMatchMaking', require('./queries/user/MatchMaking'));
    };

    initQueries();
};

DateGate.prototype.callQuery = function(name, args, fn) {
    if (!(name in this.queries))
        throw new Error('Undefined query');

    var query = new BaseQuery(this.core);
    var handler = this.queries[name];

    args.push(fn);

    return handler.apply(query, args);
};

module.exports = DateGate;

