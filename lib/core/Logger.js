"use strict";

var bunyan = require('bunyan');
var path = require('path');

var Logger = function(config) {
    this.config = config;

    var bunyan_streams = [];

    var silent = !!process.env.SILENT;

    if (!silent) {
        bunyan_streams.push({
            type: 'stream',
            stream: process.stdout,
            level: 'trace'
        });

        bunyan_streams.push({
            type: 'file',
            path: this.getLogPath(),
            level: 'info'
        });
    }

    this.logger = bunyan.createLogger({
        name: 'logger',
        streams: bunyan_streams
    });
};

Logger.prototype.formatBunyanMeta = function(meta) {
    if (!meta) meta = {};

    if (typeof meta !== 'object') {
        meta = {metadata: meta};
    }

    return meta;
};

Logger.prototype.getLogPath = function() {
    if (this.logPath)
        return this.logPath;

    var now = new Date();

    var fileName = (now.getMonth() + 1) + '_' + now.getDate() + '.log';

    this.logPath = path.join(process.cwd(), this.config.path, fileName);

    return this.logPath;
};

Logger.prototype.debug = function (message, meta) {
    this.logger.debug(this.formatBunyanMeta(meta), message);
};

Logger.prototype.info = function (message, meta) {
    this.logger.info(this.formatBunyanMeta(meta), message);
};

Logger.prototype.warn = function (message, meta) {
    this.logger.warn(this.formatBunyanMeta(meta), message);
};

Logger.prototype.error = function (message, meta) {
    this.logger.error(this.formatBunyanMeta(meta), message);
};

Logger.prototype.fatal = function (message, meta) {
    this.logger.fatal(this.formatBunyanMeta(meta), message);
};

module.exports = Logger;


