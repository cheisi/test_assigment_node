"use strict";

var path = require('path');

var express = require('express');
var session = require('express-session');
var bodyParser = require("body-parser");
var hbs = require("express-hbs");

var Config = require("./Config");
var Logger = require("./Logger");
var Db = require("./Db");
var Auth = require("./Auth");

var DataGate = require("../data/DataGate");

var HtmlHandler = require('../handlers/base/HtmlHandler');
var JsonHandler = require('../handlers/base/JsonHandler');

var Server = function() {
    var self = this;

    this.app = express();

    this.config = new Config();

    // we dont have environments in config..
    if (process.env.TEST_ENV) {
        var dbConfig = this.config.db()
        dbConfig.db = dbConfig.test_db;
    }

    this.logger = new Logger(this.config.logger());
    this.db = new Db(this);
    this.auth = new Auth(this);

    this.dataGate = new DataGate(this);

    process.on('exit', function() {
        self.logger.info("Process closed");
    });

    process.on('SIGQUIT', function() {
        self.logger.warn("Try to close process with SIGQUIT");
        self.stop();
    });

    process.on('SIGTERM', function() {
        self.logger.warn("Close process with SIGTERM");
        process.exit();
    });

    if (!process.env.TEST_ENV) {
        process.on('uncaughtException', function (e) {
            self.logger.error("Uncaught Exception:\r\n" + e.toString());
            process.exit();
        });
    }
};

Server.prototype.init = function(rootPath) {
    this.app.use(express.static(rootPath + '/public'));

    this.app.use(bodyParser.urlencoded({ extended: false }));

    this.app.use(session({
        secret: 'orly',
        saveUninitialized: false,
        resave: false
    }));


    // init hbs

    var views = path.join(rootPath, '/views');

    this.app.engine('hbs', hbs.express4({
        defaultLayout: path.join(views, 'layouts/main.hbs')
    }));

    this.app.set('views', views);

    this.app.set('view engine', 'hbs');

    // auth

    this.app.all('/oauth2callback', this.auth.oauth2callback.bind(this.auth));
    this.app.all('/logout', this.auth.logout.bind(this.auth));
    this.app.all('/login', this.auth.oauth2.bind(this.auth));

    // init routes

    this.htmlRoute('/', 'index');

    this.jsonRoute('/api/change-level', require('../handlers/ChangeLevelHandler'));
    this.jsonRoute('/api/change-rank', require('../handlers/ChangeRankHandler'));
    this.jsonRoute('/api/match-making', require('../handlers/MatchMakingHandler'));
};

Server.prototype.run = function() {
    var port = this.config.app().port;

    this.logger.info('Start listening on port ' + port);

    this.server = this.app.listen(port, '0.0.0.0');
};

Server.prototype.stop = function() {
    if (this.server) {
        this.server.close();
        this.server = null;
    }
};

Server.prototype.htmlRoute = function(route, view) {
    this.app.all(route, HtmlHandler.bind({
        core: this,
        view: view
    }));
};

Server.prototype.jsonRoute = function(route, handler) {
    this.app.post(route, this.auth.validate.bind(this.auth));

    if (handler)
        this.app.post(route, handler.bind(this));

    this.app.all(route, JsonHandler.bind(this));
};

module.exports = Server;