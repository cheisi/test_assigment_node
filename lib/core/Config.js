"use strict";

var Config = function() {
    var config = {
        app: {
            port: 8080
        },
        db: {
            user: 'postgres',
            pass: '',
            host: 'localhost',
            db: 'rd_test_assignment',
            test_db: 'rd_test_assignment_test'
        },
        logger: {
            path: '/logs'
        },
        oauth: {
            id: '857409179685-ml67jdu10at63ar337602ro7k42ksmo7.apps.googleusercontent.com',
            secret: 'oH4sjmSoPiyA7gpYanpOYb1c',
            redirect: 'http://localhost:8080/oauth2callback'
        }
    };

    this.app = function() {
        return config.app;
    };

    this.db = function() {
        return config.db;
    };

    this.logger = function() {
        return config.logger;
    };

    this.oauth = function() {
        return config.oauth;
    }
};

module.exports = Config;