"use strict";

var async = require('async');

var google = require('googleapis');
var OAuth2 = google.auth.OAuth2;

var userinfo = google.oauth2("v2").userinfo.v2;

var Auth = function(core) {
    this.core = core;
    this.config = this.core.config.oauth();

    this.oauth2Client = new OAuth2(
        this.config.id,
        this.config.secret,
        this.config.redirect
    );
};

Auth.prototype.oauth2 = function(req, res, next) {
    if (req.session.user)
        res.redirect('/');

    var url = this.oauth2Client.generateAuthUrl({
        scope: ['email']
    });

    res.redirect(url);
};

Auth.prototype.oauth2callback = function(req, res, next) {

    var self = this;

    var logger = this.core.logger;

    logger.debug('OAuth2 callback');

    async.waterfall( [

        function(next) {
            if (req.query.error)
                return next(req.query.error);

            var code = req.query.code;

            self.oauth2Client.getToken(code, next);
        },

        function(tokens, obj, next) {
            self.oauth2Client.setCredentials(tokens);

            req.session.code = req.query.code;

            userinfo.me.get({auth: self.oauth2Client}, next);
        },

        function(profile, obj, next) {
            self.getUser(profile.email, profile.name, next);
        },

        function(user, next) {
            logger.debug('User authorized');

            req.session.user = user;

            next();
        }

    ],  function(error) {
        if (error)
            req.session.error = error;

        res.redirect('/');
    });

};

Auth.prototype.logout = function(req, res) {
    delete req.session.user;

    res.redirect('/');
};

Auth.prototype.getUser = function(email, name, fn) {
    var self = this;

    this.core.dataGate.callQuery('userGetOrCreate', [email, name],  function(error, user) {
        if (error) {
            self.logger.error('Cant auth user ' + email + ': ' + error.toString());
            return fn(error);
        }

        fn(null, user);
    });
};

Auth.prototype.validate = function(req, res, next) {
    if (!req.session.user || !req.session.user.id)
        return next('Unauthorized');

    next();
};

module.exports = Auth;
