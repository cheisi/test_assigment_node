"use strict";

var pg = require('pg');
var fs = require('fs');

var Db = function(core, silent) {
    this.core = core;
    this.silent = silent;

    var config = this.core.config.db();

    this.connectionString = 'postgres://' + config.user + ':' + config.pass + '@' + config.host + '/' + config.db;
};

Db.prototype.query = function(query, fn) {
    var self = this;

    pg.connect(this.connectionString, function(error, client, done) {
        if (error) {
            self.core.logger.error('DB: error fetching client from pool: ' + error);
            return fn();
        }

        if (!self.silent)
            self.core.logger.debug('DB: running query: ' + query);

        client.query(query.toString(), function(error, result) {
            done();

            if (error) {
                self.core.logger.error('DB: ' + error + "\r\n" + query);
                return fn(error);
            }

            fn(null, result && result.rows ? result.rows : []);
        });
    });
};

Db.prototype.script = function(file, fn) {
    var self = this;

    var script = fs.readFileSync(file, {encoding: 'utf8'});

    pg.connect(this.connectionString, function(error, client, done) {
        if (error) {
            self.core.logger.error('DB: error fetching client from pool: ' + error);
            return fn();
        }

        if (!self.silent)
            self.core.logger.debug('DB: running script: ' + file);

        client.query(script, function(error) {
            done();

            if (error) {
                self.core.logger.error('DB: ' + error + "\r\n" + file);
                return fn(error);
            }

            fn();
        });
    });
};

module.exports = Db;