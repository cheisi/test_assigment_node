"use strict";

var path = require('path');
var fs = require('fs');

var DbMigrator = function(core, rootPath) {
    this.core = core;
    this.path = path.join(rootPath, 'migrations');
};

DbMigrator.prototype.getLastMigration = function (fn) {

    var db = this.core.db;

    var query = "select MAX(name) as name from migrations;";

    db.query(query, function (error, rows) {
        if (error)
            return fn(error);

        fn(null, rows && rows.length ? rows[0].name : null);
    });
};

DbMigrator.prototype.migrate = function (files, fn) {
    var self = this;

    if (!files.length) {
        self.core.logger.info('Migrations done');
        return fn();
    }

    var db = this.core.db;

    var file = files.pop();

    self.core.logger.debug('Process ' + file);

    db.script(path.join(self.path, 'scheme', file), function (error) {
        if (error)
            return fn(error);

        var query = "insert into migrations (name) values ('" + file + "');";

        db.query(query, function (error) {
            if (error)
                return fn(error);

            self.migrate(files, fn);
        });

    });
};

DbMigrator.prototype.run = function (fn) {
    var self = this;

    this.core.db.script(path.join(self.path, 'create_migrations.sql'), function () {

        self.getLastMigration(function (error, name) {
            if (error)
                return fn(error);

            var files = fs.readdirSync(path.join(self.path, 'scheme')).reverse();

            if (name) {
                while (files[files.length - 1] <= name)
                    files.pop();
            }

            self.migrate(files, fn);
        });

    });

};

module.exports = DbMigrator;
