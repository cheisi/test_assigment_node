"use strict";

module.exports = function(req, res, next) {

    var id = req.session && req.session.user ? req.session.user.id : null;

    this.dataGate.callQuery('userMatchMaking', [id], function(error, opponent) {
        if (error) {
            req.error = error;
        }

        else {
            req.result = opponent;
        }

        next();
    });
};