"use strict";

module.exports = function(req, res, next) {

    var value = req.body ? req.body.value : 0;
    var id = req.session && req.session.user ? req.session.user.id : null;

    this.dataGate.callQuery('userChangeRank', [id, value], function(error, rank) {
        if (error) {
            req.error = error;
        }

        else {
            req.session.user.rank = rank; // hack?
            req.result = rank;
        }

        next();
    });
};