"use strict";

module.exports = function(req, res, next) {

    var value = req.body ? req.body.value : 0;
    var id = req.session && req.session.user ? req.session.user.id : null;

    this.dataGate.callQuery('userChangeLevel', [id, value], function(error, level) {
        if (error) {
            req.error = error;
        }

        else {
            req.session.user.level = level; // hack?
            req.result = level;
        }

        next();
    });
};