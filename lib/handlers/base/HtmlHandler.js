"use strict";

module.exports = function(req, res) {

    var result = {};

    if (req.session) {
        result.user = JSON.stringify(req.session.user);

        result.error = JSON.stringify(req.session.error);
        delete req.session.error;
    }

    res.render(this.view, result);

};