"use strict";

module.exports = function(req, res) {

    var json = {};

    if (req.error) {
        json.error = req.error;
    }
    else if (req.result) {
        json.result = req.result;
    }

    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify(json));
    res.end();

};
