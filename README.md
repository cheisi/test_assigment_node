# Node.js Test Assignment
Node version: v4.7.2 (latest v4.x)

#### Check access to db
    psql -U postgres -h localhost -c 'select 1;'
    
#### Deploy local environment
    npm install

#### Run server
    npm start

#### Run tests
    npm test

#### Clean local environment (remove db)
    psql -U postgres -h localhost -c 'drop database rd_test_assignment;drop database rd_test_assignment_test;'