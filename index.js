"use strict";

var Server = require('./lib/core/Server');

var server = new Server();
server.init(__dirname);
server.run();